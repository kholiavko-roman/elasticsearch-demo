# Elasticsearch with Node.js demo

## Project description

* `elastic-stack` demonstrates usage of Kibana
* `search` demonstrates usage of Elasticsearch for data querying (based on OSMNames)

Data used for search example can be [found here](https://github.com/OSMNames/OSMNames/releases/tag/v2.0.4)
